export class SimpleScene extends Phaser.Scene {

  preload() {
    this.load.bitmapFont('desyrel', 'assets/fonts/bitmap/desyrel.png',
      'assets/fonts/bitmap/desyrel.xml');
  }

  create() {
    this.rectHeader = new Phaser.Geom.Rectangle(0, 0, this.game.canvas.width, 100);
    this.gHeader = this.add.graphics({ fillStyle: { color: 0x0000ff } });
    this.gHeader.fillRectShape(this.rectHeader);

    //this.rectUser = new Phaser.Geom.Rectangle(this.game.canvas.width - 200, 100, 200, this.game.canvas.heigth - 100);
    this.rectUser = new Phaser.Geom.Rectangle(this.game.canvas.width - 200, 100, 200, 800);
    this.gUser = this.add.graphics({ fillStyle: { color: 0x00cc00 } });
    this.gUser.fillRectShape(this.rectUser);

    this.locationText = this.add.bitmapText(10, 10, 'desyrel', 'SimpleScene', 16);
    this.footerLine = this.add.bitmapText(this.game.canvas.width - 150, this.game.canvas.height - 25, 'desyrel', 'coded by o00ogrfxo00o', 12);
    this.userText = this.add.bitmapText(this.game.canvas.width - 150, 50, 'desyrel', 'Charakter online', 16);


  }

  drawHeader() {
    //console.log("drawHeader");
  }

  drawMain() {
    //console.log("drawMain");
  }

  drawUsers() {
    //console.log("drawUsers");
  }

  drawChat() {
    //console.log("drawChat");
  }

  drawFooter() {
    //console.log("drawFooter");
  }

  update(time, delta) {
    this.drawHeader();
    this.drawMain();
    this.drawUsers();
    this.drawChat();
    this.drawFooter();
  }
}
