import { SimpleScene } from './simple-scene';

export class TownGateScene extends SimpleScene {
  preload() {
    super.preload();
    //this.load.image("tiles", "../assets/tilesets/overworld_tileset_grass.png");
    //this.load.tilemapTiledJSON("map", "../assets/tilemaps/town-gate.json");
  }

  create() {
    super.create();
    this.locationText.text = 'TownGate';
    // this.add.text(10, 10, 'town-gate-scene', { fill: '#0f0' });
    // this.add.image(100, 200, 'cokecan');
    //const map = this.make.tilemap({ key: "map", tileWidth: 16, tileHeight: 16 });
    //const tileset = map.addTilesetImage("overworld_tileset_grass", "tiles");

    // Parameters: layer name (or index) from Tiled, tileset, x, y
    //const belowLayer = map.createStaticLayer("ground", tileset, 0, 0);
    //const worldLayer = map.createStaticLayer("world", tileset, 0, 0);
    //const aboveLayer = map.createStaticLayer("roof", tileset, 0, 0);

    //worldLayer.setCollisionByProperty({ collides: true });
  }

  update(time, delta) {
    // Runs once per frame for the duration of the scene
    super.update(time, delta);
  }
}
