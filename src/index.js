import 'phaser';

import { TownGateScene } from './scenes/town-gate-scene';

const gameConfig = {
  width: 1024,
  height: 768,
  scene: TownGateScene,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 } // Top down game, so no gravity
    }
  }
};

new Phaser.Game(gameConfig);
